# Poeta Android Code Convention v1.0
## Source Convention
#### Package names:
- Package names are all `lowercase`, with consecutive words simply concatenated together (`no underscores`).
 
*For example:*

```kotlin
//Use
com.example.deepspace

//Avoid 
com.example.deepSpace 
//or 
com.example.deep_space
```

#### Class names:
- Class names are written in `UpperCamelCase`. 
- Class names are typically nouns or noun phrases. 

*For example:*
  
```java
Character, ImmutableList.
```


#### Class member ordering
```java 
1. Constants
2. Fields
3. Constructors
4. Override methods (match the component of lifecycle) and callbacks (public -> private)
5. Public methods
6. Private methods
7. Inner classes or interfaces
```
#### Constant names
Constant names use `CONSTANT_CASE`: `all uppercase letters`, with each word separated from the next by `a single underscore`.
#####
|Element	Field  |  Name Prefix |
|---|---|
|  SharedPreferences |  PREF_ |
| Bundle  |  BUNDLE_ |
| Fragment Arguments  |  ARGUMENT_ |
| Intent Extra  | EXTRA_  |
|  Intent Action | ACTION_  |

*For example:*

```java 

// Note the value of the field is the same as the name to avoid duplication issues
static final String PREF_EMAIL = "PREF_EMAIL";
static final String BUNDLE_AGE = "BUNDLE_AGE";
static final String ARGUMENT_USER_ID = "ARGUMENT_USER_ID";

// Intent-related items use full package name as value
static final String EXTRA_SURNAME = "com.myapp.extras.EXTRA_SURNAME";
static final String ACTION_OPEN_USER = "com.myapp.action.ACTION_OPEN_USER";
```

#### Field names 
- Local variable names are written in `lowerCamelCase`.
- These names are typically nouns or noun phrases. For example, `computedValues` or `index`.
- The varialble names should be named meaningfully. Avoid using the initial of the name or an un-meaningful name.

For example: 

```Java
//Avoid: 
 int tx; // Transition x

//Use:
 int transitionX;
```

#### Method names
- Method names are written in `lowerCamelCase`.
- Method names are typically verbs or verb phrases. For example, `sendMessage` or `stop`.
- The method names should be named meaningfully. Avoid using the initial of the name or an un-meaningful name.

For example: 

```java
//Avoid:
 public void setDivTranx()
 
//Use:
 public void setDividerTransitionX()
```

#### Parameter names
- Parameter names are written in `lowerCamelCase`.
- One-character parameter names in public methods should be avoided. 

*For example:*

```java 
//Use:
public void login(userName: String, password: String)

//Avoid: 
public void login(usn: String, pwd: String)
```

#### Parameter ordering in methods
- Try to push context as first parameter, also callBack is last.

*For example:*

```java 
//Context always goes first
public User loadUser(Context context, int userId);

// Callbacks always go last
public void loadUserAsync(Context context, int userId, UserCallback callback);
```

##  Resources Convention
####  Naming conventions for drawables:
|Asset Type   | Prefix  | Example  |
|---|---|---|
| Action bar  | ab_  | ab_stacked.9.png  |
| Button  |  btn_ | btn_send_pressed.9.png  |
| Dialog  | dialog_  | dialog_top.9.png  |
|  Divider | divider_  |  divider_horizontal.9.png |
|  Icon | ic_  | ic_star.png  |
|  Menu | menu_  | menu_submenu_bg.9.png  |
| Notification  |  notification_ | notification_bg.9.png  |
| Tabs  | tab_  | tab_pressed.9.png  |

####  Naming conventions for icons
| Asset Type | Prefix  | Example  |
|---|---|---|
|  Icons | ic_  |  ic_star.png |
| Launcher icons | ic_launcher  | ic_launcher_calendar.png  |
|  Menu and Action Bar icons |  ic_menu | ic_menu_archive.png  |
| Status bar icons	  | ic_stat_notify  |  ic_stat_notify_msg.png |
|  Tab icons	 | ic_tab  | ic_tab_recent.png  |
|  Dialog icons	 | ic_dialog  | ic_dialog_info.png  |
 	
####  Naming conventions for selector states
|  State | Suffix  |Example   |
|---|---|---|
|  Normal | _normal  | btn_order_normal.9.png  |
|  Pressed | _pressed  |  btn_order_pressed.9.png |
|  Focused | _focused  | btn_order_focused.9.png  |
| Disabled  | _disabled  | btn_order_disabled.9.png  |
|  Selected | _selected  | btn_order_selected.9.png  |

####  Layout files
| Component  | Class Name  | Layout Name  |
|---|---|---|
| Activity  | UserProfileActivity  | activity_user_profile.xml  |
| Fragment  | SignUpFragment  | fragment_sign_up.xml  |
| Dialog  |  ChangePasswordDialog |  dialog_change_password.xml |
| AdapterView item  | ---  |  item_person.xml |
| Partial layout  | ---  |  partial_stats_bar.xml |

####   View's ID Convention.
| Element  | Prefix  |
|---|---|
| Button  | btn  |
| EditText  | et  |
|  TextView | tv  |
| ProgressBar  | pb  |
| Checkbox  |  cb |
|  RadioButton | rb  |
| ToggleButton  |  tb |
| Spinner  | spn  |
|  Menu | mnu  |
| ListView  | lv  |
| GalleryView  | gv  |
| LinearLayout  |  ll |
| RelativeLayout  | rl  |
| RecyclerView  | rv  |
| ConstraintLayout  | cl  |
 For example : `btnLogIn`
 
####  Values files
Should be plural: `strings.xml`, `styles.xml`, `colors.xml`, `dimens.xml`, `attrs.xml`

####  Strings
String names start with a prefix that identifies the section they belong to. For example `registration_email_hint` or `registration_name_hint`. If a string doesn't belong to any section, then you should follow the rules below:

| Prefix  |  Description |
|---|---|
| error_  | An error message  |
|  msg_ | A regular information message  |
| title_  | A title, i.e. a dialog title  |
| action_  |  An action such as "Save" or "Create" |

#### And Keep in mind: 
`Alway run Optimize Import & Reformat Code`
#
#
#
#


